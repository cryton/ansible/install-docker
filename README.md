# Ansible role - Install Docker
This role provides installation of Docker with the option to update the Docker daemon configuration.

## Supported platforms

| Platform | version   |
|----------|-----------|
| Debian   | \>=11     |
| Kali     | \>=2022.1 |

## Requirements

- Root access (specify `become` directive as a global or while invoking the role)
    ```yaml
    become: yes
    ```
- Ansible variables (do not disable `gather_facts` directive)

## Parameters
The parameters and their defaults can be found in the `defaults/main.yml` file.

| variable                    | description                                                            |
|-----------------------------|------------------------------------------------------------------------|
| docker_daemon_configuration | Update the Docker daemon configuration file (/etc/docker/daemon.json). |

## Examples

### Usage
```yaml
  roles:
    - role: install-docker
      become: yes

```

```yaml
  roles:
    - role: install-docker
      become: yes
      docker_daemon_configuration:
        mtu: 1442
```

### Inclusion

[https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies)
```yaml
- name: install-docker
  src: https://gitlab.ics.muni.cz/cryton/ansible/install-docker.git
  version: "master"
  scm: git
```
